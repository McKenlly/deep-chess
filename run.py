from get_data import parse_pgn
from train import train


DATASET_NAME = 'dataset.pgn'
NEURAL_NETWORK_NAME = 'sample.pkl'

dataset = open(DATASET_NAME)
    
white, black = parse_pgn(dataset)
# print(len(white))
# print(white.shape)

model = train(white, black, NEURAL_NETWORK_NAME)