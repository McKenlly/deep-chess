import tensorflow as tf
import tfdeploy as td
import numpy as np
import math
import random

#сколько нужно записей
# TOTAL_MLP = 2000

TOTAL_MLP = 24

BS_MLP = 50
EPOCHS_MLP = 201 
RATE_MLP = 0.005
DECAY_MLP = 0.98

BIAS = 0.15

N_INPUT = 769 
ENCODING_1 = 600 
ENCODING_2 = 400 
ENCODING_3 = 200
ENCODING_4 = 100

HIDDEN_1 = 200
HIDDEN_2 = 400 
HIDDEN_3 = 200
HIDDEN_4 = 100 
N_OUT = 2
#количество в томе записей
# VOLUME_SIZE = 2000
VOLUME_SIZE = 24

def weight_variable(n_in, n_out):
  cur_dev = math.sqrt(3.0/(n_in+n_out))
  initial = tf.truncated_normal([n_in, n_out], stddev=cur_dev)
  return tf.Variable(initial)

def bias_variable(n_out):
  initial = tf.constant(BIAS, shape=[n_out])
  return tf.Variable(initial)

def getBatchMLP(whiteWins, blackWins, start, size):
	xR = []
	lR = []
	
	for i in range(start,start+size):
		if random.random() > 0.5:
			elem = [whiteWins[i], blackWins[i]]
			elem_l = [1,0]
		else:
			elem = [blackWins[i], whiteWins[i]]
			elem_l = [0,1]
		xR.append(elem)
		lR.append(elem_l)
	return (xR, lR)

weights = {
	'e1' : weight_variable(N_INPUT, ENCODING_1),	
	'e2' : weight_variable(ENCODING_1, ENCODING_2),	
	'e3' : weight_variable(ENCODING_2, ENCODING_3),	
	'e4' : weight_variable(ENCODING_3, ENCODING_4),	
	'd1' : weight_variable(ENCODING_4, ENCODING_3),	
	'd2' : weight_variable(ENCODING_3, ENCODING_2),	
	'd3' : weight_variable(ENCODING_2, ENCODING_1),	
	'd4' : weight_variable(ENCODING_1, N_INPUT),	
	'w1' : weight_variable(HIDDEN_1, HIDDEN_2),	
	'w2' : weight_variable(HIDDEN_2, HIDDEN_3),	
	'w3' : weight_variable(HIDDEN_3, HIDDEN_4),	
	'w4' : weight_variable(HIDDEN_4, N_OUT)	
}

biases = {
	'e1' : bias_variable(ENCODING_1),	
	'e2' : bias_variable(ENCODING_2),	
	'e3' : bias_variable(ENCODING_3),	
	'e4' : bias_variable(ENCODING_4),	
	'd1' : bias_variable(ENCODING_3),	
	'd2' : bias_variable(ENCODING_2),	
	'd3' : bias_variable(ENCODING_1),	
	'd4' : bias_variable(N_INPUT),	
	'b1' : bias_variable(HIDDEN_2),	
	'b2' : bias_variable(HIDDEN_3),	
	'b3' : bias_variable(HIDDEN_4),	
	'out' : bias_variable(N_OUT)	
}

#матричный слой * вес + биас_перем
def fully_connected(current_layer, weight, bias):
    next_layer = tf.add(tf.matmul(current_layer, weight), bias)
    next_layer = tf.maximum(0.01*next_layer, next_layer)
    return next_layer

def encode(c, weights, biases, level):    
    e1 = fully_connected(c, weights['e1'], biases['e1'])
    if level == 1:
        return e1

    e2 = fully_connected(e1, weights['e2'], biases['e2'])
    if level == 2:
        return e2

    e3 = fully_connected(e2, weights['e3'], biases['e3'])
    if level == 3:
        return e3
    
    e4 = fully_connected(e3, weights['e4'], biases['e4'])
    return e4

def decode(d, weights, biases, level):
    pred  = fully_connected(d, weights['d'+ str(5-level)], biases['d' + str(5-level)])
    return pred

def singleEncode(c, weights, biases, level):
    pred  = fully_connected(c, weights['e'+ str(level)], biases['e' + str(level)])
    return pred

def model(games, weights, biases, first_board, second_board):
    first_board = games[:,0,:]
    second_board = tf.squeeze(tf.slice(games, [0,1,0], [-1, 1, -1]), squeeze_dims=[1])
    [first_board, second_board] = tf.unstack(games, axis=1)
    
    firstboard_encoding = encode(first_board, weights, biases, 4)
    secondboard_encoding = encode(second_board, weights, biases, 4)
    
    # слои нейросети
    h_1 = tf.concat([firstboard_encoding,secondboard_encoding], 1)
    h_2 = fully_connected(h_1, weights['w1'], biases['b1'])
    h_3 = fully_connected(h_2, weights['w2'], biases['b2'])
    h_4 = fully_connected(h_3, weights['w3'], biases['b3'])
    pred = tf.add(tf.matmul(h_4, weights['w4']), biases['out'], name="output")
    return pred


def train(white_wins, black_wins, name_model):
    learning_rate = tf.placeholder(tf.float32, shape=[])
    raw_x = tf.placeholder(tf.float32, shape=[None, N_INPUT])
    x = tf.placeholder(tf.float32, shape=[None, 2, N_INPUT], name="input")
    first_board = tf.placeholder(tf.float32, shape=[None, N_INPUT])
    second_board = tf.placeholder(tf.float32, shape=[None, N_INPUT])
    
    y_ = tf.placeholder(tf.float32, shape=[None,2])
    
    y = model(x, weights, biases, first_board, second_board)

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y, labels=y_))
    mlp_train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cross_entropy)
    init = tf.initialize_all_variables()
    saver = tf.train.Saver()
    
    with tf.Session() as sess:
        sess.run(init)
        total_batch = 2
        for epoch in range(EPOCHS_MLP):
            cur_rate = RATE_MLP * (DECAY_MLP**epoch)
            white_wins = np.random.permutation(white_wins)
            black_wins = np.random.permutation(black_wins)
            cost = 0.0
            for i in range(total_batch): 
                batch_xs, batch_ys = getBatchMLP(white_wins, black_wins, i*BS_MLP, BS_MLP)
                _, cost = sess.run([mlp_train_step, cross_entropy], feed_dict={x: batch_xs, y_:batch_ys, learning_rate: cur_rate})
            print("MLP Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(cost))
            tdmodel = td.Model()
            tdmodel.add(y, sess)
            tdmodel.save(name_model)
        return name_model
