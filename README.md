# Инструкция по запуску

```
python3 -m venv env
source env/bin/activate

pip3 install numpy
pip3 install python-chess
pip3 install setuptools==45.1.0
pip3 install grpcio==1.26.0
pip3 install tensorflow==1.13.0rc1
pip3 install tfdeploy
```


# Для обучения нейросети

Укажите названия файлов

``` 
DATASET_NAME = 'dataset.pgn'
NEURAL_NETWORK_NAME = 'sample.pkl' 
```
Запуск

```python3 run.py```

# Для запуска игры

Укажите названия нейросетей

```
NEURAL_NETWORK_NAME_ONE = 'sample.pkl'
NEURAL_NETWORK_NAME_TWO = 'sample2.pkl'
```
```python3 game.py```
