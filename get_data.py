import numpy as np
import chess
import chess.pgn
import random
import pickle
from util import *


# Максимальное количество снимков
BATCH_SIZE = 5000
# Количество снимков в одной партии
SPG = 20

def get_good_moves(node):
	total = 0
	good = []
	while not node.is_end():
		next_node = node.variation(0)
		x = (node.board().san(next_node.move))
		total = total + 1
		if 'x' in x:
			if total > 6:
				good.append(total)
		node = next_node
	return good


def traverse(node, moves, arr, cur_index):
    total = 1 
    arr_size = len(arr) 
    board = chess.Board()
    while not node.is_end():
        move = next(iter(node.mainline_moves()))
        board.push(chess.Move.from_uci(str(move)))
        if total in moves:
            #Снимок одного хода.
            arr[cur_index] = bitifyFEN(beautifyFEN(board.fen()))
            cur_index = cur_index + 1
            if cur_index == arr_size:
                return cur_index
            moves.remove(total)
            if not moves:
                return cur_index
        next_node = node.variation(0)
        node = next_node
        total = total + 1
    return cur_index 


def add_game_data(game, arr, cur_index):
    goodmoves = get_good_moves(game)
    picked = []
    for i in range(SPG):
        if not goodmoves:
            break
        #Добавление рандомного хода игрока.      
        nu = random.choice(goodmoves)
        goodmoves.remove(nu)
        picked.append(nu)
    #Если пустая запись, то возвращаем индекс
    if not picked:
        return cur_index
    cur_index = traverse(game, picked, arr, cur_index)
    return cur_index

# pgn -- файл, который парсим, изначально открыт.
def parse_pgn(pgn):
	white_wins = np.zeros((BATCH_SIZE, 769))
	black_wins = np.zeros((BATCH_SIZE, 769))
	white_index = 0
	black_index = 0

	# pgn = open(DATASET_NAME)
	while white_index < BATCH_SIZE and black_index < BATCH_SIZE:    
		g = chess.pgn.read_game(pgn)
		if not g:
			break
        # равномерное распределение белых и черных в каждом мини-датасете
		if g.headers["Result"] == "1-0" and white_index < BATCH_SIZE:
			white_index = add_game_data(g, white_wins, white_index)

		elif g.headers["Result"] == "0-1" and black_index < BATCH_SIZE:
			black_index = add_game_data(g, black_wins, black_index)
		
	min_len = min(white_index, black_index)
	white_wins = white_wins[0:min_len]
	black_wins = black_wins[0:min_len]
	return (white_wins, black_wins)