import chess
import tfdeploy as td
import itertools
import copy
import random
from util import *

N_INPUT = 769

NEURAL_NETWORK_NAME_ONE = 'sample.pkl'
NEURAL_NETWORK_NAME_TWO = 'sample2.pkl'

model = td.Model(NEURAL_NETWORK_NAME_ONE)
x = model.get("input")
y = model.get("output")

model2 = td.Model(NEURAL_NETWORK_NAME_TWO)
x2 = model2.get("input")
y2 = model2.get("output")


def compare_boards(first, second, x, y):
    X = np.zeros((1, 2, N_INPUT))
    X[0][0] = bitifyFEN(beautifyFEN(first.fen()))
    X[0][1] = bitifyFEN(beautifyFEN(second.fen()))

    result = y.eval(feed_dict={x: X})

    return result[0][0]
    
def alphabeta(board, depth, alpha, beta, white, orig_board):
    if depth == 0:
        if white:
            return compare_boards(board, orig_board, x, y), None
        else:
            return compare_boards(board, orig_board, x2, y2), None
    if white:
        v = -100 
        moves = board.generate_legal_moves()
        moves = list(moves)
        best_move = None
        for move in moves:
            new_board = board.copy()
            new_board.push(move)
            candidate_v, _ = alphabeta(new_board, depth - 1, alpha, beta, False, orig_board)
            if candidate_v >= v:
                v = candidate_v
                best_move = move
            else:
                pass
            alpha = max(alpha, v)
            if beta <= alpha:
                break
        return v, best_move
    else:
        v = 100 # very (relatively) large number
        moves = board.generate_legal_moves()
        moves = list(moves)
        best_move = None
        for move in moves:
            new_board = board.copy()
            new_board.push(move)
            candidate_v, _ = alphabeta(new_board, depth - 1, alpha, beta, True, orig_board)
            if candidate_v <= v:
                v = candidate_v
                best_move = move
            else:
                pass
            beta = min(beta, v)
            if beta <= alpha:
                break
        return v, best_move


def playGame():
	moveTotal = 0
	board = chess.Board()
	prediction = 0
    
	while board.is_game_over() == False:
		print("Move {}".format(moveTotal))
        
		print(board)
		if moveTotal % 2 == 1:
			prediction, move = alphabeta(board, 1, -100, 100, True, board)
			board.push(move)
		else:
			prediction, move = alphabeta(board, 1, -100, 100, False, board)

			board.push(move)
		# print(prediction)
		moveTotal = moveTotal+1
	
	print("Finish")
	print(board)
	print("Game is over")
	print(board.result())

		
playGame()